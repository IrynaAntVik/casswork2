import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Classwork2 {

    public void chetnoe() {
        for (int i = 0; i <= 40; i++) {
            if (i % 2 == 0) {
                System.out.print(i + " ");
            }
        }
    }

    public void nechetnoe() {
        for (int i = 0; i <= 40; i++) {
            if (i % 2 != 0) {
                System.out.print(i + " ");
            }
        }
    }

    private BufferedReader reader;
    String code = null;
    double a = 0;
    double b = 0;

    public void parametri() {
        try {
            System.out.println("Введите значение a");
            a = Double.parseDouble(reader.readLine());
            System.out.println("Введите значение b");
            b = Double.parseDouble(reader.readLine());
        } catch (NumberFormatException n) {
            System.out.println("a и b должны быть действительными числами");
            parametri();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void switchcalculator() {
        reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите код операции");
        try {
            code = reader.readLine();
            while (!code.equals("exit")) {
                switch (code) {
                    case "+":
                        parametri();
                        System.out.println(a + b);
                        break;
                    case "-":
                        parametri();
                        System.out.println(a - b);
                        break;
                    case "*":
                        parametri();
                        System.out.println(a * b);
                        break;
                    case "/":
                        parametri();
                        if (b ==0){
                            System.out.println("b не должен быть равен 0 при выборе кода операции /");
                            parametri();
                        }
                        System.out.println(a / b);
                        break;
                    case "%":
                        parametri();
                        System.out.println(a % b);
                        break;
                    case "^":
                        parametri();
                        System.out.println(Math.pow(a, b));
                        break;
                    case "root":
                        parametri();
                        System.out.println(Math.pow(a, 1.0 / b));
                        break;
                    case "rand":
                        parametri();
                        System.out.println(a + Math.random() * b);
                        break;
                    case "help":
                        System.out.println("Список доступных операций:");
                        System.out.println("+ Сложение A и B");
                        System.out.println("- Вычитание B из A");
                        System.out.println("* Умножение A на B");
                        System.out.println("/ Деление A на B");
                        System.out.println("% Деление по модулю A на B");
                        System.out.println("^ Возведение A в степень B");
                        System.out.println("root Вычисление из А корня степени B");
                        System.out.println("rand Вывод случайного числа от A до B");
                        System.out.println("help Вывод списка операций");
                        System.out.println("exit Выход из программы");
                        switchcalculator();
                        break;
                    default:
                        System.out.println("Неверный код операции. Введите help для помощи");
                        switchcalculator();
                }
                System.out.println("Введите код операции");
                code = reader.readLine();
            }
        } catch (NumberFormatException n) {
            switchcalculator();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
